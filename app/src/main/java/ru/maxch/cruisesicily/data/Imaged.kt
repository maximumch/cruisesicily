package ru.maxch.cruisesicily.data

interface Imaged {
     val image: String?
}
package ru.maxch.cruisesicily.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.maxch.cruisesicily.data.DataRepository

class LocationsViewModelFactory(
    private val repository: DataRepository,
    private val regionId:Int?,
    private val islandId:Int?
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) = LocationsViewModel(repository, regionId,islandId) as T
}
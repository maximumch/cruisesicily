package ru.maxch.cruisesicily.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Matchers
import org.junit.*
import org.junit.runner.RunWith
import ru.maxch.cruisesicily.utilites.getValue

@RunWith(AndroidJUnit4::class)
class RegionDaoTest {
    private lateinit var database: AppDatabase
    private lateinit var regionDao: RegionDao
    private val regionA = Region(1, "America", "America is ...", null, "https://million-wallpapers.ru/wallpapers/1/24/353400406347254.jpg")
    private val regionB = Region(2, "North America", "North America is ...", 1, "https://million-wallpapers.ru/wallpapers/1/24/353400406347254.jpg")
    private val regionC = Region(3, "South America", "South America ...",1, "https://million-wallpapers.ru/wallpapers/1/24/353400406347254.jpg" )

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb(){
        val context = InstrumentationRegistry.getTargetContext()
        database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        regionDao = database.regionDao()
        regionDao.insertAll(listOf(regionB,regionC,regionA))
    }

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    fun testGetRegions() {
        val regions = getValue(regionDao.getRegions())

        Assert.assertThat(regions[0], Matchers.equalTo(regionA))
        Assert.assertThat(regions[1], Matchers.equalTo(regionB))
        Assert.assertThat(regions[2], Matchers.equalTo(regionC))
    }

    @Test
    fun testGetRegion(){
        Assert.assertThat(getValue(regionDao.getRegion(regionB.id)), Matchers.equalTo(regionB))
    }

}
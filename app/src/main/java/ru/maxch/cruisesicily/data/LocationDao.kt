package ru.maxch.cruisesicily.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface LocationDao {
    @Query("SELECT * FROM location ORDER BY name")
    fun getLocations(): LiveData<List<Location>>

    @Query("SELECT * FROM location WHERE id = :locationId")
    fun getLocation(locationId:Int): LiveData<Location>

    @Query("SELECT * FROM location WHERE region =:regionId")
    fun getLocationsForRegion(regionId:Int): LiveData<List<Location>>

    @Query("SELECT * FROM location WHERE island = :islandId")
    fun getLocationsForIsland(islandId: Int): LiveData<List<Location>>



    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(locations: List<Location>)


}
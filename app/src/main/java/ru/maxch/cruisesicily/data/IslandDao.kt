package ru.maxch.cruisesicily.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * The Data Access Object for the Island class.
 */

@Dao
interface IslandDao {
    @Query("SELECT * FROM island ORDER BY name")
    fun getIslands():LiveData<List<Island>>

    @Query("SELECT * FROM island WHERE id = :islandId")
    fun getIsland(islandId:Int): LiveData<Island>

    @Query("SELECT * FROM island WHERE region = :regionId")
    fun getIslandsForRegion(regionId:Int): LiveData<List<Island>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(islands: List<Island>)


}
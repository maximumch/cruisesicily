package ru.maxch.cruisesicily.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "island")
 data class Island(
    @PrimaryKey val id: Int,
    val name: String,
    val description: String? = null,
    val lon: Double,
    val lat: Double,
    override val image: String? = null,
    val map: String? = null,
    val region: Int
 ):Imaged
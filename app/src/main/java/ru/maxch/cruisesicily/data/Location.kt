package ru.maxch.cruisesicily.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "location")
data class Location(
    @PrimaryKey val id: Int,
    val name: String,
    val description: String? = null,
    val lat: Double,
    val lon: Double,
    override val map: String? = null,
    val island: Int? = null,
    val region: Int,
    override val image: String? = null
):Imaged,Mapped
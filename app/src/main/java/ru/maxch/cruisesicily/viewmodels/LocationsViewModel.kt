package ru.maxch.cruisesicily.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import ru.maxch.cruisesicily.data.DataRepository
import ru.maxch.cruisesicily.data.Location

class LocationsViewModel internal constructor(regionRepository: DataRepository, regionId:Int?, islandId:Int?) : ViewModel() {
    val locations: LiveData<List<Location>>
    init{
        locations = if(regionId!=null){
            regionRepository.getLocationsForRegion(regionId)
        }else if(islandId!=null){
            regionRepository.getLocationsForIsland(islandId)
        }else{
            regionRepository.getLocations()
        }
    }
}
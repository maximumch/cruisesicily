package ru.maxch.cruisesicily.data

import androidx.lifecycle.LiveData
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext

class DataRepository private constructor(
    private val regionDao: RegionDao,
    private val locationDao: LocationDao,
    private val islandDao: IslandDao
) {
    fun getRegions() = regionDao.getRegions()
    fun getIslands() = islandDao.getIslands()
    fun getLocations() = locationDao.getLocations()
    fun getRegions(superRegionId:Int) = regionDao.getRegions(superRegionId)
    fun getRegion(regionId:Int) = regionDao.getRegion(regionId)
    fun getIsland(islandId: Int) = islandDao.getIsland(islandId)
    fun getLocation(locationId: Int) = locationDao.getLocation(locationId)

    suspend fun insertAll(regions: List<Region>){
        withContext(IO){
            regionDao.insertAll(regions)
        }
    }
    fun getLocationsForRegion(regionId:Int) = locationDao.getLocationsForRegion(regionId)
    fun getIslandsForRegion(regionId:Int) = islandDao.getIslandsForRegion(regionId)
    fun getRegionsForRegion(regionId:Int) = regionDao.getRegionsForRegion(regionId)
    fun getLocationsForIsland(islandId: Int) = locationDao.getLocationsForIsland(islandId)



    companion object {
        // For Singleton instantiation
        @Volatile private var instance: DataRepository? = null

        fun getInstance(regionDao: RegionDao, locationDao:LocationDao, islandDao: IslandDao) =
            instance ?: synchronized(this) {
                instance ?: DataRepository(regionDao,locationDao,islandDao).also { instance = it }
            }
    }
}
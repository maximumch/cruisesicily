package ru.maxch.cruisesicily

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import ru.maxch.cruisesicily.adapters.RegionAdapter
import ru.maxch.cruisesicily.databinding.FragmentRegionsBinding
import ru.maxch.cruisesicily.utilites.InjectorUtils
import ru.maxch.cruisesicily.viewmodels.RegionsViewModel

class RegionsFragment: Fragment() {
    private lateinit var viewModel: RegionsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentRegionsBinding.inflate(inflater,container,false)
        val context = context ?: return binding.root

        val args = arguments

        val str = if(args!=null) RegionsFragmentArgs.fromBundle(args).superRegionId else null

        val superRegionId:Int? = if(str!=null) str.toInt() else null

        val factory = InjectorUtils.provideRegionsViewModelFactory(context,superRegionId)
        viewModel = ViewModelProviders.of(this, factory).get(RegionsViewModel::class.java)

        val adapter = RegionAdapter()
        binding.regionList.adapter = adapter
        subscribeUi(adapter)
        return binding.root

    }

    private fun subscribeUi(adapter: RegionAdapter) {
        viewModel.regions.observe(viewLifecycleOwner, Observer { regions ->
            if (regions != null) adapter.submitList(regions)
        })
    }


}
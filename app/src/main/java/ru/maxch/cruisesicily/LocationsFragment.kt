package ru.maxch.cruisesicily

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import ru.maxch.cruisesicily.adapters.LocationAdapter
import ru.maxch.cruisesicily.databinding.FragmentLocationsBinding
import ru.maxch.cruisesicily.utilites.InjectorUtils

import ru.maxch.cruisesicily.viewmodels.LocationsViewModel

class LocationsFragment: Fragment() {
    private lateinit var viewModel: LocationsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentLocationsBinding.inflate(inflater,container,false)
        val context = context ?: return binding.root
        val args = arguments
        val regionStr = if(args!=null) LocationsFragmentArgs.fromBundle(args).regionId else null
        val regionId:Int? = regionStr?.toIntOrNull()
        val islandStr = if(args!=null) LocationsFragmentArgs.fromBundle(args).islandId else null
        val islandId:Int? = islandStr?.toIntOrNull()
        val factory = InjectorUtils.provideLocationsViewModelFactory(context,regionId,islandId )
        viewModel = ViewModelProviders.of(this, factory).get(LocationsViewModel::class.java)

        val adapter = LocationAdapter()
        binding.locationList.adapter = adapter
        subscribeUi(adapter)
        return binding.root

    }

    private fun subscribeUi(adapter: LocationAdapter) {
        viewModel.locations.observe(viewLifecycleOwner, Observer { locations ->
            if (locations != null) adapter.submitList(locations)
        })
    }

}
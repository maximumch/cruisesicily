package ru.maxch.cruisesicily

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import at.blogc.android.views.ExpandableTextView
import ru.maxch.cruisesicily.databinding.FragmentIslandDetailBinding
import ru.maxch.cruisesicily.databinding.FragmentLocationDetailBinding
import ru.maxch.cruisesicily.utilites.InjectorUtils
import ru.maxch.cruisesicily.utilites.min

import ru.maxch.cruisesicily.viewmodels.IslandDetailViewModel
import ru.maxch.cruisesicily.viewmodels.LocationDetailViewModel

class LocationDetailFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val locationId = LocationDetailFragmentArgs.fromBundle(arguments!!).locationId

        val factory = InjectorUtils.provideLocationDetailViewModelFactory(requireActivity(),locationId.toInt())
        val viewModel = ViewModelProviders.of(this,factory).get(LocationDetailViewModel::class.java)

        val binding = DataBindingUtil.inflate<FragmentLocationDetailBinding>(
            inflater, R.layout.fragment_location_detail, container, false).apply {
            this.viewModel = viewModel
            lifecycleOwner = this@LocationDetailFragment

        }

        binding.descriptionChangeSize.setOnClickListener {
            binding.description.toggle()
        }

        binding.description.addOnExpandListener(object: ExpandableTextView.OnExpandListener{
            override fun onExpand(view: ExpandableTextView) {
                binding.descriptionChangeSize.text = getText(R.string.expandable_view_less)
            }

            override fun onCollapse(view: ExpandableTextView) {
                binding.descriptionChangeSize.text = getText(R.string.expandable_view_more)
            }
        })

        viewModel.location.observe(this, Observer{
            val description = it.description
            val view = binding.description
            if (description != null) {
                val text = HtmlCompat.fromHtml(description, HtmlCompat.FROM_HTML_MODE_COMPACT)
                view.text = text
                view.movementMethod = LinkMovementMethod.getInstance()
            } else {
                view.text = ""
            }
        })

        return binding.root
    }

//    private fun setDescriptionHeight(descriptionTextView: TextView, isShowFull:Boolean ){
//        descriptionTextView.apply{
//            val h = if (isShowFull) {
//                lineCount * lineHeight + lineHeight / 4
//            } else {
//                val visibleLines = resources.getInteger(R.integer.short_description_lines)
//                val lines= min(visibleLines,lineCount)
//                if(lines>1) getLineBounds(lines,null) + lineHeight / 4 else 0
//            }
//            if (h > 0) height = h
//
//
//        }
//
//
//    }

}

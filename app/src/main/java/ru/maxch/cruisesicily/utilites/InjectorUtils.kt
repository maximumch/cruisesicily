package ru.maxch.cruisesicily.utilites

import android.content.Context
import ru.maxch.cruisesicily.data.AppDatabase
import ru.maxch.cruisesicily.data.DataRepository
import ru.maxch.cruisesicily.viewmodels.*

object InjectorUtils {


    fun provideRegionsViewModelFactory(
        context: Context,
        superRegionId:Int?
    ): RegionsViewModelFactory {
        val repository = getDataRepository(context)
        return RegionsViewModelFactory(repository, superRegionId)
    }

    fun provideRegionDetailViewModelFactory(
        context: Context,
        regionId:Int
    ): RegionDetailViewModelFactory {
        val repository = getDataRepository(context)
        return RegionDetailViewModelFactory(repository, regionId)
    }

    fun provideIslandsViewModelFactory(
        context: Context,
        regionId:Int?
    ): IslandsViewModelFactory {
        val repository = getDataRepository(context)
        return IslandsViewModelFactory(repository, regionId)
    }

    fun provideLocationsViewModelFactory(
        context: Context,
        regionId:Int?,
        islandId:Int?
    ): LocationsViewModelFactory {
        val repository = getDataRepository(context)
        return LocationsViewModelFactory(repository, regionId,islandId)
    }

    fun provideIslandDetailViewModelFactory(
        context: Context,
        islandId:Int
    ): IslandDetailViewModelFactory {
        val repository = getDataRepository(context)
        return IslandDetailViewModelFactory(repository, islandId)
    }

    fun provideLocationDetailViewModelFactory(
        context: Context,
        locationId:Int
    ): LocationDetailViewModelFactory {
        val repository = getDataRepository(context)
        return LocationDetailViewModelFactory(repository, locationId)
    }


    private fun getDataRepository(context: Context): DataRepository {
        val db = AppDatabase.getInstance(context)
        return DataRepository.getInstance(
            db.regionDao(),
            db.locationDao(),
            db.islandDao()
        )
    }


}
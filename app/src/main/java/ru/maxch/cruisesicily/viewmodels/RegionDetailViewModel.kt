package ru.maxch.cruisesicily.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ru.maxch.cruisesicily.data.DataRepository

class RegionDetailViewModel internal constructor(repository: DataRepository, regionId:Int): ViewModel(){
    val region = repository.getRegion(regionId)
    var isShowFullDescription = MutableLiveData<Boolean>(false)
    val locationList = repository.getLocationsForRegion(regionId)
    val islandList = repository.getIslandsForRegion(regionId)
    val regionList = repository.getRegionsForRegion(regionId)
}
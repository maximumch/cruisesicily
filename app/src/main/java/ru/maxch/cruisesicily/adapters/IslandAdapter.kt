package ru.maxch.cruisesicily.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ru.maxch.cruisesicily.IslandsFragmentDirections
import ru.maxch.cruisesicily.data.Island
import ru.maxch.cruisesicily.databinding.ListItemIslandBinding


class IslandAdapter: ListAdapter<Island, IslandAdapter.ViewHolder>(IslandDiffCallback())  {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListItemIslandBinding.inflate(
                LayoutInflater.from(parent.context), parent, false  ))

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val island = getItem(position)
        holder.apply {
            bind(createOnClickListener(island.id),island)
            itemView.tag = island
        }


    }

    private fun createOnClickListener(islandId: Int): View.OnClickListener {
        return View.OnClickListener {
            val direction = IslandsFragmentDirections.actionIslandsFragmentToIslandDetailFragment(islandId.toString())
            it.findNavController().navigate(direction)
        }
    }

    class ViewHolder(
        private val binding: ListItemIslandBinding
    ): RecyclerView.ViewHolder(binding.root) {
        fun bind(listener: View.OnClickListener, item: Island){

            binding.apply {
                clickListener = listener
                island = item
                executePendingBindings()
            }
        }
    }


}


private class IslandDiffCallback : DiffUtil.ItemCallback<Island>() {

    override fun areItemsTheSame(oldItem: Island, newItem: Island): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Island, newItem: Island): Boolean {
        return oldItem == newItem
    }
}
package ru.maxch.cruisesicily.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "region")
data class Region(
    @PrimaryKey val id:Int,
    val name:String,
    val description:String? = null,
    val superRegion:Int? = null,
    override val image:String? = null
    ):Imaged
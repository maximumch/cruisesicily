package ru.maxch.cruisesicily.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.maxch.cruisesicily.data.DataRepository

class LocationDetailViewModelFactory(
    private val repository: DataRepository,
    private val locationId:Int
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) = LocationDetailViewModel(repository, locationId) as T
}
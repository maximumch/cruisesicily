package ru.maxch.cruisesicily.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Matchers.equalTo
import org.junit.After
import org.junit.Assert.assertThat

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Before
import org.junit.Rule
import ru.maxch.cruisesicily.utilites.getValue

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class IslandDaoTest {
    private lateinit var database: AppDatabase
    private lateinit var islandDao: IslandDao
    private val islandA = Island(1, "Java island", "Java island is warm and sunny",120.0,10.0,"https://million-wallpapers.ru/wallpapers/1/24/353400406347254.jpg","https://avatars.mds.yandex.net/get-pdb/1016956/158951cb-438d-4b3f-ac5b-44a79508621e/s1200?webp=false",1)
    private val islandB = Island(2, "Kotlin island", "Kotlin island is cold and rainy",35.0,60.0,"https://million-wallpapers.ru/wallpapers/1/24/353400406347254.jpg","https://avatars.mds.yandex.net/get-pdb/1016956/158951cb-438d-4b3f-ac5b-44a79508621e/s1200?webp=false",1)
    private val islandC = Island(3, "Avalon island", "Avalon island is a myth",120.0,10.0,"https://million-wallpapers.ru/wallpapers/1/24/353400406347254.jpg","https://avatars.mds.yandex.net/get-pdb/1016956/158951cb-438d-4b3f-ac5b-44a79508621e/s1200?webp=false",1)

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb(){
        val context = InstrumentationRegistry.getTargetContext()
        database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        islandDao = database.islandDao()

        islandDao.insertAll(listOf(islandB,islandC,islandA))
    }

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    fun testGetIslands() {
        val islands = getValue(islandDao.getIslands())

        assertThat(islands[0], equalTo(islandC))
        assertThat(islands[1], equalTo(islandA))
        assertThat(islands[2], equalTo(islandB))
    }

    @Test
    fun testGetIsland(){
        assertThat(getValue(islandDao.getIsland(islandB.id)), equalTo(islandB))
    }
}

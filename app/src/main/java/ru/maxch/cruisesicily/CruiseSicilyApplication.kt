package ru.maxch.cruisesicily

import android.app.Application

class CruiseSicilyApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        SyncIntentService.startSync(this, "sicily","june","ru")
    }
}
package ru.maxch.cruisesicily.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ru.maxch.cruisesicily.data.DataRepository

class LocationDetailViewModel internal constructor(repository: DataRepository, locationId:Int): ViewModel(){
    val location = repository.getLocation(locationId)
    var isShowFullDescription = MutableLiveData<Boolean>(false)
}
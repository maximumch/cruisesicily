package ru.maxch.cruisesicily

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import at.blogc.android.views.ExpandableTextView
import ru.maxch.cruisesicily.databinding.FragmentIslandDetailBinding
import ru.maxch.cruisesicily.databinding.FragmentRegionDetailBinding
import ru.maxch.cruisesicily.utilites.InjectorUtils
import ru.maxch.cruisesicily.utilites.min

import ru.maxch.cruisesicily.viewmodels.IslandDetailViewModel
import ru.maxch.cruisesicily.viewmodels.RegionDetailViewModel

class IslandDetailFragment: Fragment() {
    //private lateinit var viewModel: RegionDetailViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val islandId = IslandDetailFragmentArgs.fromBundle(arguments!!).islandId

        val factory = InjectorUtils.provideIslandDetailViewModelFactory(requireActivity(),islandId.toInt())
        val viewModel = ViewModelProviders.of(this,factory).get(IslandDetailViewModel::class.java)

        val binding = DataBindingUtil.inflate<FragmentIslandDetailBinding>(
            inflater, R.layout.fragment_island_detail, container, false).apply {
            this.viewModel = viewModel
            lifecycleOwner = this@IslandDetailFragment

        }

        binding.descriptionChangeSize.setOnClickListener {
            binding.description.toggle()
        }

        binding.description.addOnExpandListener(object: ExpandableTextView.OnExpandListener{
            override fun onExpand(view: ExpandableTextView) {
                binding.descriptionChangeSize.text = getText(R.string.expandable_view_less)
            }

            override fun onCollapse(view: ExpandableTextView) {
                binding.descriptionChangeSize.text = getText(R.string.expandable_view_more)
            }
        })

        binding.layoutLocationList.setOnClickListener {
            val direction = IslandDetailFragmentDirections.actionIslandDetailFragmentToLocationsFragment("",islandId)
            it.findNavController().navigate(direction)
        }

        viewModel.island.observe(this, Observer{
            val description = it.description
            val view = binding.description
            if (description != null) {
                val text = HtmlCompat.fromHtml(description, HtmlCompat.FROM_HTML_MODE_COMPACT)
                view.text = text
                view.movementMethod = LinkMovementMethod.getInstance()
            } else {
                view.text = ""
            }
        })


        viewModel.locationList.observe(this, Observer{
            if (!it.isNullOrEmpty()) {
                binding.layoutLocationList.visibility = View.VISIBLE
                binding.locationListSize.text="(${it.size})"
            } else {
                binding.layoutLocationList.visibility = View.GONE
            }
        })

        return binding.root
    }


}



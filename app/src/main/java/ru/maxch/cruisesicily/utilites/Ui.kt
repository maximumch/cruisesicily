package ru.maxch.cruisesicily.utilites

import android.util.Log
import android.widget.TextView
import ru.maxch.cruisesicily.R

fun ToDeletesetDescriptionHeight(descriptionTextView: TextView, isShowFull:Boolean ){
    descriptionTextView.apply{
        Log.d("happyAnimation","isShowFull = $isShowFull" )
        Log.d("happyAnimation","text length = ${text?.length}" )
        Log.d("happyAnimation","lineCount = $lineCount" )
        Log.d("happyAnimation","lineHeight = $lineHeight" )
        val h = if (isShowFull) {
            if(lineCount>0) {
                val lineBounds = getLineBounds(lineCount-1,null)
                lineBounds + lineHeight / 4
            }else{
                0
            }
        } else {
            val visibleLines = resources.getInteger(R.integer.short_description_lines)
            val lines= min(visibleLines,lineCount)
            Log.d("happyAnimation","lines = $lines" )
            if(lines>1) {
                val lineBounds = getLineBounds(lines,null)
                Log.d("happyAnimation","lineBounds = $lineBounds" )
                lineBounds + lineHeight / 4
            } else{
                0
            }
        }
        Log.d("happyAnimation","h = $h" )
        if (h > 0) height = h
    }
}
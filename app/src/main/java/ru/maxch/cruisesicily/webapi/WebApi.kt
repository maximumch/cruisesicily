package ru.maxch.cruisesicily.webapi

import android.util.Log
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import ru.maxch.cruisesicily.data.Island
import ru.maxch.cruisesicily.data.Location
import ru.maxch.cruisesicily.data.Region
import javax.net.ssl.SSLContext
import javax.net.ssl.X509TrustManager

interface WebApi {
    @GET("TestServlet/regions")
    fun getRegions(
        @Query("login") login:String,
        @Query("password") password:String,
        @Query("lang") lang:String
    ): Call<List<Region>>

    @GET("TestServlet/islands")
    fun getIslands(
        @Query("login") login:String,
        @Query("password") password:String,
        @Query("lang") lang:String
    ): Call<List<Island>>

    @GET("TestServlet/locations")
    fun getLocations(
        @Query("login") login:String,
        @Query("password") password:String,
        @Query("lang") lang:String
    ): Call<List<Location>>




    companion object {
        //private const val BASE_URL = "http://5.45.67.39:8080/TestServlet/regions?login=sicily&password=june&lang=ru"
        private const val BASE_URL = "http://5.45.67.39:8080/"
        fun create(): WebApi {
            val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
                                Log.d("API", it)
            })

            logger.level = HttpLoggingInterceptor.Level.BASIC
            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()
            return Retrofit.Builder()
                .baseUrl(HttpUrl.parse(BASE_URL))
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WebApi::class.java)



        }




//        fun create(sslContext: SSLContext, x509TrustManager: X509TrustManager): UploaderApi = create(HttpUrl.parse(BASE_URL)!!, sslContext, x509TrustManager)
//        fun create(httpUrl: HttpUrl, sslContext: SSLContext, x509TrustManager: X509TrustManager): UploaderApi {
//            val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
//                Log.d("API", it)
//            })
//            logger.level = HttpLoggingInterceptor.Level.BASIC
//
//            val client = OkHttpClient.Builder()
//                .addInterceptor(logger)
//                .sslSocketFactory(sslContext.socketFactory, x509TrustManager)
//                .build()
//            return Retrofit.Builder()
//                .baseUrl(httpUrl)
//                .client(client)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build()
//                .create(UploaderApi::class.java)
//        }
    }
}
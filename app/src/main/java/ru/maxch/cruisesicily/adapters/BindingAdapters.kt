package ru.maxch.cruisesicily.adapters

import android.text.method.LinkMovementMethod
import android.widget.ImageView
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import ru.maxch.cruisesicily.viewmodels.RegionDetailViewModel

@BindingAdapter("imageFromURL")
fun bindImageFromUrl(view: ImageView, imageUrl:String?){
    if(!imageUrl.isNullOrEmpty()){
        Glide.with(view.context)
            .load(imageUrl)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(view)
    }
}

@BindingAdapter("renderHtml")
fun bindRenderHtml(view: TextView, description:String?) {
    if (description != null) {
        val text = HtmlCompat.fromHtml(description, HtmlCompat.FROM_HTML_MODE_COMPACT)
        view.text = text
        view.movementMethod = LinkMovementMethod.getInstance()
    } else {
        view.text = ""
    }
}

@BindingAdapter("descriptionHeight")
fun bindDescriptionHeight(view: TextView, isShowFullDescription: Boolean) {
    view.maxLines = 5
//    if (description != null) {
//        view.text = HtmlCompat.fromHtml(description, HtmlCompat.FROM_HTML_MODE_COMPACT)
//        view.movementMethod = LinkMovementMethod.getInstance()
//    } else {
//        view.text = ""
//    }
}
package ru.maxch.cruisesicily.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ru.maxch.cruisesicily.LocationsFragmentDirections
import ru.maxch.cruisesicily.data.Island
import ru.maxch.cruisesicily.data.Location
import ru.maxch.cruisesicily.databinding.ListItemIslandBinding
import ru.maxch.cruisesicily.databinding.ListItemLocationBinding

class LocationAdapter: ListAdapter<Location, LocationAdapter.ViewHolder>(LocationDiffCallback())  {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListItemLocationBinding.inflate(
                LayoutInflater.from(parent.context), parent, false  ))

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val location = getItem(position)
        holder.apply {
            bind(createOnClickListener(location.id),location)
            itemView.tag = location
        }


    }

    private fun createOnClickListener(locationId: Int): View.OnClickListener {
        return View.OnClickListener {
            val direction = LocationsFragmentDirections.actionLocationsFragmentToLocationDetailFragment(locationId.toString())
            it.findNavController().navigate(direction)
        }
    }

    class ViewHolder(
        private val binding: ListItemLocationBinding
    ): RecyclerView.ViewHolder(binding.root) {
        fun bind(listener: View.OnClickListener, item: Location){

            binding.apply {
                clickListener = listener
                location = item
                executePendingBindings()
            }
        }
    }


}


private class LocationDiffCallback : DiffUtil.ItemCallback<Location>() {

    override fun areItemsTheSame(oldItem: Location, newItem: Location): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Location, newItem: Location): Boolean {
        return oldItem == newItem
    }
}
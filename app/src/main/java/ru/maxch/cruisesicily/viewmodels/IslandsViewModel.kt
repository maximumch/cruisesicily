package ru.maxch.cruisesicily.viewmodels

import androidx.lifecycle.ViewModel
import ru.maxch.cruisesicily.data.DataRepository

class IslandsViewModel internal constructor(regionRepository: DataRepository, regionId:Int?) : ViewModel() {
    val islands = if(regionId!=null) regionRepository.getIslandsForRegion(regionId) else regionRepository.getIslands()
}
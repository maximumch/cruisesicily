package ru.maxch.cruisesicily.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Matchers
import org.junit.*
import org.junit.runner.RunWith
import ru.maxch.cruisesicily.utilites.getValue

@RunWith(AndroidJUnit4::class)
class LocationDaoTest {
    private lateinit var database: AppDatabase
    private lateinit var locationDao: LocationDao
    private val locationA = Location(1, "Catania", "Catania ...",120.0,10.0,"https://million-wallpapers.ru/wallpapers/1/24/353400406347254.jpg", 1,1,"https://avatars.mds.yandex.net/get-pdb/1016956/158951cb-438d-4b3f-ac5b-44a79508621e/s1200?webp=false")
    private val locationB = Location(2, "Palermo", "Palermo",120.0,10.0,"https://million-wallpapers.ru/wallpapers/1/24/353400406347254.jpg", 2,2,"https://avatars.mds.yandex.net/get-pdb/1016956/158951cb-438d-4b3f-ac5b-44a79508621e/s1200?webp=false")
    private val locationC = Location(3, "Siracusa", "Siracusa",120.0,10.0,"https://million-wallpapers.ru/wallpapers/1/24/353400406347254.jpg", 3,3,"https://avatars.mds.yandex.net/get-pdb/1016956/158951cb-438d-4b3f-ac5b-44a79508621e/s1200?webp=false")

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb(){
        val context = InstrumentationRegistry.getTargetContext()
        database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        locationDao = database.locationDao()
        locationDao.insertAll(listOf(locationB,locationC,locationA))
    }

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    fun testGetIslands() {
        val islands = getValue(locationDao.getLocations())

        Assert.assertThat(islands[0], Matchers.equalTo(locationA))
        Assert.assertThat(islands[1], Matchers.equalTo(locationB))
        Assert.assertThat(islands[2], Matchers.equalTo(locationC))
    }

    @Test
    fun testGetIsland(){
        Assert.assertThat(getValue(locationDao.getLocation(locationB.id)), Matchers.equalTo(locationB))
    }

}
package ru.maxch.cruisesicily.webapi

import androidx.test.runner.AndroidJUnit4
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class WebApiTest {


    @Test
    fun getIslandsTest(){
        val webApi = WebApi.create()
        val call = webApi.getIslands("sicily","june","ru")
        var response = call.execute()
        assertTrue(response.isSuccessful)
        val islands = response.body()
        assertNotNull(islands)
        assertTrue(islands!!.isNotEmpty())

    }

    @Test
    fun getRegionsTest(){
        val webApi = WebApi.create()
        val call = webApi.getRegions("sicily","june","ru")
        var response = call.execute()
        assertTrue(response.isSuccessful)
        val regions = response.body()
        assertNotNull(regions)
        assertTrue(regions!!.isNotEmpty())

    }

    @Test
    fun getLocationsTest(){
        val webApi = WebApi.create()
        val call = webApi.getLocations("sicily","june","ru")
        var response = call.execute()
        assertTrue(response.isSuccessful)
        val locations = response.body()
        assertNotNull(locations)
        assertTrue(locations!!.isNotEmpty())

    }


}
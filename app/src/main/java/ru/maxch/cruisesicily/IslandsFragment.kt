package ru.maxch.cruisesicily

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import ru.maxch.cruisesicily.adapters.IslandAdapter
import ru.maxch.cruisesicily.databinding.FragmentIslandsBinding
import ru.maxch.cruisesicily.utilites.InjectorUtils
import ru.maxch.cruisesicily.viewmodels.IslandsViewModel



class IslandsFragment: Fragment() {
    private lateinit var viewModel: IslandsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentIslandsBinding.inflate(inflater,container,false)
        val context = context ?: return binding.root
        val args = arguments
        val str = if(args!=null) IslandsFragmentArgs.fromBundle(args).regionId else null
        val regionId:Int? = str?.toInt()
        val factory = InjectorUtils.provideIslandsViewModelFactory(context,regionId)
        viewModel = ViewModelProviders.of(this, factory).get(IslandsViewModel::class.java)

        val adapter = IslandAdapter()
        binding.islandList.adapter = adapter
        subscribeUi(adapter)





        return binding.root

    }

    private fun subscribeUi(adapter: IslandAdapter) {
        viewModel.islands.observe(viewLifecycleOwner, Observer { islands ->
            if (islands != null) adapter.submitList(islands)
        })
    }

}
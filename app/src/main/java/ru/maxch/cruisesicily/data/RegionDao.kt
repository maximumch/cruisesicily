package ru.maxch.cruisesicily.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface RegionDao {
    @Query("SELECT * FROM region WHERE superRegion is null ORDER BY name")
    fun getRegions(): LiveData<List<Region>>

    @Query("SELECT * FROM region WHERE superRegion = :superRegionId ORDER BY name")
    fun getRegions(superRegionId:Int): LiveData<List<Region>>

    @Query("SELECT * FROM region WHERE id = :regionId")
    fun getRegion(regionId:Int): LiveData<Region>

    @Query("SELECT * FROM region WHERE superRegion = :regionId")
    fun getRegionsForRegion(regionId:Int): LiveData<List<Region>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(regions: List<Region>)

}
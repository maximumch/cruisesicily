package ru.maxch.cruisesicily.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ru.maxch.cruisesicily.data.DataRepository

class IslandDetailViewModel internal constructor(repository: DataRepository, islandId:Int): ViewModel(){
    val island = repository.getIsland(islandId)
    var isShowFullDescription = MutableLiveData<Boolean>(false)
    val locationList = repository.getLocationsForIsland(islandId)
}
package ru.maxch.cruisesicily.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil

import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ru.maxch.cruisesicily.BR.clickListener
import ru.maxch.cruisesicily.RegionsFragmentDirections

import ru.maxch.cruisesicily.data.Region
import ru.maxch.cruisesicily.databinding.ListItemRegionBinding


class RegionAdapter: ListAdapter<Region,RegionAdapter.ViewHolder>(RegionDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ListItemRegionBinding.inflate(
            LayoutInflater.from(parent.context), parent, false  ))

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val region = getItem(position)
        holder.apply {
            bind(createOnClickListener(region.id),region)
            itemView.tag = region
        }


    }

    private fun createOnClickListener(regionId: Int): View.OnClickListener {
        return View.OnClickListener {
            val direction = RegionsFragmentDirections.actionRegionsFragmentToRegionDetailFragment(regionId.toString())
            it.findNavController().navigate(direction)
        }
    }

    class ViewHolder(
        private val binding: ListItemRegionBinding
    ): RecyclerView.ViewHolder(binding.root) {
        fun bind(listener: View.OnClickListener, item: Region){

            binding.apply {
                clickListener = listener
                region = item
                executePendingBindings()
            }




        }


    }

}

private class RegionDiffCallback : DiffUtil.ItemCallback<Region>() {

    override fun areItemsTheSame(oldItem: Region, newItem: Region): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Region, newItem: Region): Boolean {
        return oldItem == newItem
    }
}
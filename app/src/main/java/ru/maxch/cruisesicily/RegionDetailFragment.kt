package ru.maxch.cruisesicily

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import at.blogc.android.views.ExpandableTextView
import ru.maxch.cruisesicily.databinding.FragmentRegionDetailBinding
import ru.maxch.cruisesicily.utilites.InjectorUtils

import ru.maxch.cruisesicily.viewmodels.RegionDetailViewModel


class RegionDetailFragment: Fragment() {
    //private lateinit var viewModel: RegionDetailViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val regionId = RegionDetailFragmentArgs.fromBundle(arguments!!).regionId

        val factory = InjectorUtils.provideRegionDetailViewModelFactory(requireActivity(),regionId.toInt())
        val viewModel = ViewModelProviders.of(this,factory).get(RegionDetailViewModel::class.java)




        val binding = DataBindingUtil.inflate<FragmentRegionDetailBinding>(
            inflater, R.layout.fragment_region_detail, container, false).apply {
                this.viewModel = viewModel
                lifecycleOwner = this@RegionDetailFragment

        }

        binding.descriptionChangeSize.setOnClickListener {
            binding.regionDescription.toggle()
        }

        binding.regionDescription.addOnExpandListener(object: ExpandableTextView.OnExpandListener{
            override fun onExpand(view: ExpandableTextView) {
                binding.descriptionChangeSize.text = getText(R.string.expandable_view_less)
            }

            override fun onCollapse(view: ExpandableTextView) {
                binding.descriptionChangeSize.text = getText(R.string.expandable_view_more)
            }

        })

        binding.layoutRegionList.setOnClickListener {
            val direction = RegionDetailFragmentDirections.actionRegionDetailFragmentToRegionsFragment(regionId)
            it.findNavController().navigate(direction)
        }

        binding.layoutIslandList.setOnClickListener {
            val direction = RegionDetailFragmentDirections.actionRegionDetailFragmentToIslandsFragment(regionId.toString())
            it.findNavController().navigate(direction)
        }

        binding.layoutLocationList.setOnClickListener {
            val direction = RegionDetailFragmentDirections.actionRegionDetailFragmentToLocationsFragment(regionId.toString(),"")
            it.findNavController().navigate(direction)
        }

        viewModel.region.observe(this, Observer{
            val description = it.description
            val view = binding.regionDescription
            if (description != null) {
                val text = HtmlCompat.fromHtml(description, HtmlCompat.FROM_HTML_MODE_COMPACT)
                view.text = text
                view.movementMethod = LinkMovementMethod.getInstance()

            } else {
                view.text = ""
            }
        })

        viewModel.islandList.observe(this, Observer{
                if (!it.isNullOrEmpty()) {
                    binding.layoutIslandList.visibility = View.VISIBLE
                    binding.islandListSize.text="(${it.size})"
                } else {
                    binding.layoutIslandList.visibility = View.GONE
                }
        })

        viewModel.locationList.observe(this, Observer{
            if (!it.isNullOrEmpty()) {
                binding.layoutLocationList.visibility = View.VISIBLE
                binding.locationListSize.text="(${it.size})"
            } else {
                binding.layoutLocationList.visibility = View.GONE
            }
        })

        viewModel.regionList.observe(this, Observer{
            if (!it.isNullOrEmpty()) {
                binding.layoutRegionList.visibility = View.VISIBLE
                binding.regionListSize.text="(${it.size})"
            } else {
                binding.layoutRegionList.visibility = View.GONE
            }
        })
        viewModel.region.observe(this, Observer{
            if (it.description?.isNotBlank()==true)   {
                binding.layoutDescription.visibility = View.VISIBLE
            } else {
                binding.layoutDescription.visibility = View.GONE
            }
        })

        return binding.root
    }

//    private fun setDescriptionHeight(descriptionTextView: TextView, isShowFull:Boolean ){
//        descriptionTextView.apply{
//            val h = if (isShowFull) {
//                lineCount * lineHeight + lineHeight / 4
//            } else {
//                val visibleLines = resources.getInteger(R.integer.short_description_lines)
//                val lines= min(visibleLines,lineCount)
//                if(lines>1) getLineBounds(lines,null) + lineHeight / 4 else 0
//            }
//            if (h > 0) height = h
//
//
//        }
//
//
//    }

}



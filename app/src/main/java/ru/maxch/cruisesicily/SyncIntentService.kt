package ru.maxch.cruisesicily

import android.app.IntentService
import android.content.Intent
import android.content.Context
import android.util.Log
import androidx.core.net.toUri
import com.bumptech.glide.Glide
import ru.maxch.cruisesicily.data.AppDatabase
import ru.maxch.cruisesicily.data.Imaged
import ru.maxch.cruisesicily.data.Island
import ru.maxch.cruisesicily.webapi.WebApi
import java.lang.Exception
import ru.maxch.cruisesicily.data.Mapped as Mapped


private const val ACTION_SYNC = "ru.maxch.cruisesicily.action.SYNC"
private const val EXTRA_LOGIN = "ru.maxch.cruisesicily.extra.LOGIN"
private const val EXTRA_PASSWORD = "ru.maxch.cruisesicily.extra.PASSWORD"
private const val EXTRA_LANG = "ru.maxch.cruisesicily.extra.LANG"

class SyncIntentService : IntentService("SyncIntentService") {

    override fun onHandleIntent(intent: Intent?) {
        when (intent?.action) {
            ACTION_SYNC -> {
                val login = intent.getStringExtra(EXTRA_LOGIN)
                val password = intent.getStringExtra(EXTRA_PASSWORD)
                val lang = intent.getStringExtra(EXTRA_LANG)
                handleSync(login, password, lang)
            }
        }
    }

    private fun handleSync(login: String, password: String, lang:String) {
        val islandDao = AppDatabase.getInstance(this).islandDao()
        val locationDao = AppDatabase.getInstance(this).locationDao()
        val regionDao = AppDatabase.getInstance(this).regionDao()
        val webApi = WebApi.create()
        try {
            val islandsCall = webApi.getIslands(login, password, lang)
            var islandsResponse = islandsCall.execute()
            if (islandsResponse.isSuccessful) {
                islandsResponse.body()?.also {
                    islandDao.insertAll(it)
                    cacheImaged(it)
                }
            }

            val regionsCall = webApi.getRegions(login, password, lang)
            var regionsResponse = regionsCall.execute()
            if (regionsResponse.isSuccessful) {
                regionsResponse.body()?.also {
                    regionDao.insertAll(it)
                    cacheImaged(it)
                }
            }

            val locationsCall = webApi.getLocations(login, password, lang)
            var locationsResponse = locationsCall.execute()
            if (locationsResponse.isSuccessful) {
                locationsResponse.body()?.also {
                    locationDao.insertAll(it)
                    cacheImaged(it)
                    cacheMapped(it)

                }

            }

        }catch (e:Exception){
            Log.e("SyncIntentService",e.message)
        }

    }

    private fun cacheImaged(imagedList:List<Imaged>){
        for(imaged in imagedList) {
            imaged.image?.also { url -> cacheImage(url) }
        }
    }

    private fun cacheMapped(imagedList:List<Mapped>){
        for(imaged in imagedList){
            imaged.map?.also {url->cacheImage(url)}
        }
    }

    private fun cacheImage(url:String){
        Glide.with(this.applicationContext)
            .load(url)
            .submit()
    }

    private fun tst(){
        Glide.with(this.applicationContext).asFile().load("ddf").submit().get().toUri()
    }

    companion object {
        @JvmStatic
        fun startSync(context: Context, login: String, password: String, lang:String) {
            val intent = Intent(context, SyncIntentService::class.java).apply {
                action = ACTION_SYNC
                putExtra(EXTRA_LOGIN, login)
                putExtra(EXTRA_PASSWORD, password)
                putExtra(EXTRA_LANG, lang)
            }
            context.startService(intent)
        }
    }
}

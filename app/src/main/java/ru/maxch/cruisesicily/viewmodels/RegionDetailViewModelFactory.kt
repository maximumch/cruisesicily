package ru.maxch.cruisesicily.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.maxch.cruisesicily.data.DataRepository

class RegionDetailViewModelFactory(
    private val repository: DataRepository,
    private val regionId:Int
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) = RegionDetailViewModel(repository, regionId) as T
}